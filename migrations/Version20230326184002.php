<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230326184002 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE stock (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, produits_id INTEGER DEFAULT NULL, prix_gros INTEGER DEFAULT NULL, qte INTEGER DEFAULT NULL, CONSTRAINT FK_4B365660CD11A2CF FOREIGN KEY (produits_id) REFERENCES produit (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('CREATE INDEX IDX_4B365660CD11A2CF ON stock (produits_id)');
        $this->addSql('CREATE TABLE vente (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, produits_id INTEGER DEFAULT NULL, qte INTEGER NOT NULL, CONSTRAINT FK_888A2A4CCD11A2CF FOREIGN KEY (produits_id) REFERENCES produit (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('CREATE INDEX IDX_888A2A4CCD11A2CF ON vente (produits_id)');
        $this->addSql('CREATE TEMPORARY TABLE __temp__produit AS SELECT id, designation FROM produit');
        $this->addSql('DROP TABLE produit');
        $this->addSql('CREATE TABLE produit (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, designation VARCHAR(255) NOT NULL, reference VARCHAR(255) NOT NULL)');
        $this->addSql('INSERT INTO produit (id, designation) SELECT id, designation FROM __temp__produit');
        $this->addSql('DROP TABLE __temp__produit');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE stock');
        $this->addSql('DROP TABLE vente');
        $this->addSql('CREATE TEMPORARY TABLE __temp__produit AS SELECT id, designation FROM produit');
        $this->addSql('DROP TABLE produit');
        $this->addSql('CREATE TABLE produit (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, designation VARCHAR(255) NOT NULL, qte INTEGER NOT NULL, prix_u INTEGER NOT NULL, prix_vente INTEGER NOT NULL, cmup INTEGER NOT NULL)');
        $this->addSql('INSERT INTO produit (id, designation) SELECT id, designation FROM __temp__produit');
        $this->addSql('DROP TABLE __temp__produit');
    }
}
