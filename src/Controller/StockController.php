<?php

namespace App\Controller;

use App\Entity\Stock;
use App\Form\StockType;
use App\Repository\ProduitRepository;
use App\Repository\StockRepository;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/entree')]
class StockController extends AbstractController
{

    #[Route('/index', name: 'app_stock_index', methods: ['GET'])]
    public function index(StockRepository $stockRepository): Response
    {
        return $this->render('stock/index.html.twig', [
            'stocks' => $stockRepository->findAll(),
        ]);
    }

    #[Route('/cmup', name: 'app_stock_cmup', methods: ['GET'])]
    public function cmup(StockRepository $stockRepository): Response
    {
        return $this->render('stock/index.html.twig', [
            'stocks' => $stockRepository->findAll(),
        ]);
    }


    #[Route('/new', name: 'app_stock_new', methods: ['GET', 'POST'])]
    public function new(Request $request, StockRepository $stockRepository, ProduitRepository $produitRepository): Response
    {
        if ($request->getMethod() === 'POST') {
            $produit = $produitRepository->find($request->request->get('produit'));
            if ($produit) {
                $stock = new Stock();
                $stock->setDateAchat(new DateTime($request->request->get('date')));
                $stock->setPrixGros($request->request->get('prix'));
                $stock->setQte($request->request->get('qte'));
                $stock->setProduits($produit);
                $stockRepository->save($stock, true);

                return $this->redirectToRoute('app_stock_index', [], Response::HTTP_SEE_OTHER);
            }
        }

        return $this->renderForm('stock/new.html.twig', [
            'produits' => $produitRepository->findAll(),
        ]);
    }

    #[Route('/{id}', name: 'app_stock_show', methods: ['GET'])]
    public function show(Stock $stock): Response
    {
        return $this->render('stock/show.html.twig', [
            'stock' => $stock,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_stock_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Stock $stock, StockRepository $stockRepository, ProduitRepository $produitRepository): Response
    {
        if ($request->getMethod() === 'POST') {

            $produit = $produitRepository->find($request->request->get('produit'));
            if ($produit) {

                $stock->setDateAchat(new DateTime($request->request->get('date')));
                $stock->setPrixGros($request->request->get('prix'));
                $stock->setQte($request->request->get('qte'));
                $stock->setProduits($produit);

                $stockRepository->save($stock, true);

                return $this->redirectToRoute('app_stock_index', [], Response::HTTP_SEE_OTHER);
            }
        }

        return $this->renderForm('stock/new.html.twig', [
            'produits' => $produitRepository->findAll(),
        ]);
    }


    #[Route('/{id}', name: 'app_stock_delete', methods: ['POST'])]
    public function delete(Request $request, Stock $stock, StockRepository $stockRepository): Response
    {
        if ($this->isCsrfTokenValid('delete' . $stock->getId(), $request->request->get('_token'))) {
            $stockRepository->remove($stock, true);
        }

        return $this->redirectToRoute('app_stock_index', [], Response::HTTP_SEE_OTHER);
    }
}
