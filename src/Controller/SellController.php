<?php

namespace App\Controller;

use App\Entity\Sell;
use App\Form\SellType;
use App\Repository\SellRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/sell')]
class SellController extends AbstractController
{
    #[Route('/index', name: 'app_sell_index', methods: ['GET'])]
    public function index(SellRepository $sellRepository): Response
    {
        return $this->render('sell/index.html.twig', [
            'sells' => $sellRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_sell_new', methods: ['GET', 'POST'])]
    public function new(Request $request, SellRepository $sellRepository): Response
    {
        $sell = new Sell();
        $form = $this->createForm(SellType::class, $sell);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $sellRepository->save($sell, true);

            return $this->redirectToRoute('app_sell_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('sell/new.html.twig', [
            'sell' => $sell,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_sell_show', methods: ['GET'])]
    public function show(Sell $sell): Response
    {
        return $this->render('sell/show.html.twig', [
            'sell' => $sell,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_sell_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Sell $sell, SellRepository $sellRepository): Response
    {
        $form = $this->createForm(SellType::class, $sell);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $sellRepository->save($sell, true);

            return $this->redirectToRoute('app_sell_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('sell/edit.html.twig', [
            'sell' => $sell,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_sell_delete', methods: ['POST'])]
    public function delete(Request $request, Sell $sell, SellRepository $sellRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$sell->getId(), $request->request->get('_token'))) {
            $sellRepository->remove($sell, true);
        }

        return $this->redirectToRoute('app_sell_index', [], Response::HTTP_SEE_OTHER);
    }
}
